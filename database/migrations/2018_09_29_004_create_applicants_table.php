<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('sort_id')->default(1);
            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('job_id');
            $table->string('name');
            $table->string('short')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('details_doc')->nullable();
            $table->string('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }

    /*function uniqueMake(){
        $s = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
        return strtoupper($s);
    }*/
}
