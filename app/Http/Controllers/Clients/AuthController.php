<?php

namespace App\Http\Controllers\Clients;

use App\Http\Controllers\Controller;
use App\Models\Applicants;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function login(Request $request){
        return view('Clients.Auth.login');
    }
    public function dashboard(Request $request){
        return view('Clients.Pages.dashboard');
    }
    public function applicants(Request $request){
        return view('Clients.Pages.applicants');
    }
    public function jobs(Request $request){
        return view('Clients.Pages.jobs');
    }
    public function applicantQuestionPaper(Request $request, $short_code){

        $applicantModel = new Applicants();
        $applicantInfo = $applicantModel->where('short', $short_code)->where('is_active', 1)->get()->first();
        if($applicantInfo == null){
            abort('404');
        }


        $rv = array(
            'short_code' => $short_code
        );
        return view('Applicant.Pages.questions')->with($rv);
    }
    public function applicantEntry(Request $request){
        return view('Applicant.Pages.entry');
    }

}
