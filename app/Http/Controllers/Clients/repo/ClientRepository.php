<?php

namespace App\Http\Controllers\Clients\Repo;

use App\Models\Answers;
use App\Models\Applicants;
use App\Models\Jobs;
use App\Models\Questions;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Mockery\Exception;
use Illuminate\Support\Facades\DB;

Class ClientRepository
{

    //========================
    // Authentication
    //========================
    public function login($request)
    {
        try {
            $input = $request->input();
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required|string|min:8',
            ]);
            if ($validator->fails()) {
                return ['status' => 5000, 'error' => $validator->errors()];
            }
            $credentials = array(
                'email' => $input['email'],
                'password' => $input['password'],
                'is_active' => 1
            );
            $remember = isset($input['remember']) ? $input['remember'] : false;
            if (Auth::guard('web')->attempt($credentials, $remember)) {
                return ['status' => 2000, 'msg' => 'Login Successful'];
            } else {
                return ['status' => 5000, 'error' => 'Invalid Credentials'];
            }
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function logout($request)
    {
        try {
            Auth::guard('web')->logout();
            return ['status' => 2000, 'msg' => 'Logout Successful'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function getJobs($request)
    {
        try {
            $ownerId = Auth::guard('web')->user()->id;
            $Model = new Jobs();
            $data = $Model->where('owner_id', $ownerId)->where('is_active', 1)->orderBy('id','desc')->get()->toArray();
            return ['status' => 2000, 'data' => $data];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function getApplicants($request)
    {
        try {
            $ownerId = Auth::guard('web')->user()->id;
            $Model = new Applicants();
            $data = $Model->where('owner_id', $ownerId)->where('is_active', 1)->orderBy('id','desc')->get()->toArray();
            return ['status' => 2000, 'data' => $data];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function getApplicantShort($short_code)
    {
        try {
            $Model = new Applicants();
            $data = $Model->where('short', $short_code)->where('is_active', 1)->get()->first();
            $Model = new Questions();
            $questionData = $Model->where('owner_id', $data->owner_id)->where('job_id', $data->job_id)->where('is_active', 1)->orderBy('sort_id','asc')->get()->toArray();
            $questions = [];
            foreach ($questionData as $question){
                $Model = new Answers();
                $answer = $Model->where('applicant_id', $data->id)->where('questions_id', $question['id'])->where('is_active', 1)->get()->first();
                if($answer == null){
                    $question['answer_id'] = 0;
                    $question['answer'] = '';
                } else {
                    $question['answer_id'] = $answer->id;
                    $question['answer'] = $answer->answer;
                }
                $questions[] = $question;
            }
            $rv = array(
                'applicant' => $data,
                'questions' => $questions
            );
            return ['status' => 2000, 'data' => $rv];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function questionAnswer($request,$short_code)
    {
        try {
            $Model = new Applicants();
            $applicant = $Model->where('short', $short_code)->where('is_active', 1)->get()->first();
            if($applicant == null){
                return ['status' => 5000, 'msg' => 'Invalid Request'];
            }

            $input = $request->input();
            foreach ($input['questions'] as $question){
                if($question['answer_id'] > 0){
                    $Model = new Answers();
                    $Model->where('applicant_id', $applicant->id)
                        ->where('questions_id', $question['id'])
                        ->where('id', $question['answer_id'])
                        ->where('is_active', 1)->update([
                            'answer' => $question['answer']
                        ]);
                } else {
                    $Model = new Answers();
                    $Model->applicant_id = $applicant->id;
                    $Model->questions_id = $question['id'];
                    $Model->answer = $question['answer'];
                    $Model->created_at = Carbon::now();
                    $Model->save();
                }
            }
            return ['status' => 2000, 'msg' => 'Answers have been saved successfully'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function createApplicant($request)
    {
        try {
            $input = $request->input();
            $ownerId = Auth::guard('web')->user()->id;

            $Model = new Applicants();
            $Model->owner_id = $ownerId;
            $Model->job_id = $input['job_id'];
            $Model->name = $input['name'];
            $Model->email = isset($input['email']) ? $input['email'] : null;
            $Model->phone = isset($input['phone']) ? $input['phone'] : null;
            $Model->created_at = Carbon::now();
            $Model->save();
            $applicantId = $Model->id;


            $s = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $short_code = strtoupper($s).$applicantId;
            $Model = new Applicants();
            $Model->where('id',$applicantId)->update([
                'short' => $short_code
            ]);

            return ['status' => 2000, 'msg' => 'New applicant has been added successfully.'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function applicantEntry($request)
    {
        try {
            $input = $request->input();

            $Model = new Applicants();
            $check = $Model->where('email',$input['email'])->get()->first();
            if($check != null){
                return ['status' => 7000, 'msg' => 'Email already been used'];
            }


            $Model = new Applicants();
            $Model->owner_id = 1;
            $Model->job_id = 1;
            $Model->name = $input['name'];
            $Model->email = isset($input['email']) ? $input['email'] : null;
            $Model->phone = isset($input['phone']) ? $input['phone'] : null;
            $Model->created_at = Carbon::now();
            $Model->save();
            $applicantId = $Model->id;

            $s = substr(str_shuffle(str_repeat("abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
            $short_code = strtoupper($s).$applicantId;
            $Model = new Applicants();
            $Model->where('id',$applicantId)->update([
                'short' => $short_code
            ]);
            $data = $Model->where('id',$applicantId)->get()->first();

            return ['status' => 2000, 'data' => $data];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function applicantUpdate($request)
    {
        try {
            $input = $request->input();

            $Model = new Applicants();
            $check = $Model->where('short',$input['short'])->get()->first();
            if($check == null){
                return ['status' => 3000, 'msg' => 'Invalid Request'];
            }
            $check = $Model->where('short','!=',$input['short'])->where('email',$input['email'])->get()->first();
            if($check != null){
                return ['status' => 7000, 'msg' => 'Email already been used'];
            }

            $Model = new Applicants();
            $Model->where('id',$input['id'])->where('short',$input['short'])->update([
                'email' => $input['email'],
                'phone' => $input['phone'],
                'details_doc' => $input['details_doc'],
            ]);
            $data = $Model->where('id',$input['id'])->where('short',$input['short'])->get()->first();

            return ['status' => 2000, 'data' => $data];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function removeApplicant($request)
    {
        try {
            $input = $request->input();
            $ownerId = Auth::guard('web')->user()->id;

            $Model = new Applicants();
            $Model->where('owner_id',$ownerId)->whereIn('id',$input['ids'])->update([
                'is_active' => 0
            ]);

            return ['status' => 2000, 'msg' => 'Applicant has been removed successfully.'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }
    public function editApplicant($request)
    {
        try {
            $input = $request->input();
            $ownerId = Auth::guard('web')->user()->id;

            $Model = new Applicants();
            $Model->where('owner_id',$ownerId)->where('id',$input['id'])->update([
                'job_id' => $input['job_id'],
                'name' => $input['name'],
                'email' => $input['email'],
                'phone' => $input['phone'],
            ]);

            return ['status' => 2000, 'msg' => 'Applicant has been updated successfully.'];
        } catch (Exception $e) {
            return ['status' => 5000, 'error' => $e->getMessage()];
        }

    }


}