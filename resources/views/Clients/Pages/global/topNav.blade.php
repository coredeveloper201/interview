<nav>
    <div class="nav-wrapper">
        <ul id="nav-mobile">
            <li><a data-target="slide-out" class="sidenav-trigger" style="margin: 0;display: block;"><i class="material-icons dp48">dehaze</i></a></li>
        </ul>
        <a href="#" class="brand-logo center">Mediusware</a>
    </div>
</nav>