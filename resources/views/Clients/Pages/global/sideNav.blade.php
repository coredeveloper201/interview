<div id="app-topNav">
    <ul id="slide-out" class="sidenav">
        <li>
            <div class="user-view">
                <h4 class="center-align">Midusware</h4>
                <a href="#name"><span class="white-text name">John Doe</span></a>
                <a href="#email"><span class="white-text email">jdandturk@gmail.com</span></a>
            </div>
        </li>
        <li><a href="{{route('client.applicants.front')}}">Applicants</a></li>
        <li><a href="{{route('client.jobs.front')}}">Jobs</a></li>
        <li><div class="divider"></div></li>
        <li><a href="javascript:void(0)" @click="logout">Logout</a></li>
    </ul>
</div>

<script>
    new Vue({
        el: '#app-topNav',
        data: {
            api_url: '{{env("Client_API")}}'
        },
        methods: {
            logout: function () {
                const _this = this;
                const URL = this.api_url+'/auth/logout';
                $.ajax({
                    url: URL,
                    type: "get",
                    success: function (res) {
                        console.log(res);
                        if(parseInt(res.status) === 2000){
                            window.location.href = '{{env('client.logout.api')}}';
                        } else {
                            M.toast({html: 'Something Wrong. Please try again!'});
                        }
                    }
                });
            }
        },
        mounted(){
            $(document).ready(function(){
                $('.sidenav').sidenav();
            });
        }
    });
</script>