@extends('Layouts.clients')

@section('content')
    <div id="app-vue">
        @include('Clients.Pages.global.topNav')
        @include('Clients.Pages.global.sideNav')

        @include('Clients.Pages.global.applicants')



    </div>
@endsection