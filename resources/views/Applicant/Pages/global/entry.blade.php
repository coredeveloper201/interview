<div id="app-entry">
    <div class="container">
        <div class="row">
            <div class="col s12 m6 col offset-m3">
                <br><br>
                <form @submit.prevent="login">
                    <div class="card animated slideInUp">
                        <div class="card-content">
                            <span class="card-title">Applicant Entry</span>
                            <br>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="name" v-model="formLog.name" type="text" required class="validate">
                                    <label for="name">Name</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="email" v-model="formLog.email" type="email" required class="validate">
                                    <label for="email">Email Address</label>
                                </div>
                                <div class="input-field col s12">
                                    <input id="phone" v-model="formLog.phone" type="tel" required class="validate">
                                    <label for="phone">Contact Number</label>
                                </div>
                            </div>
                        </div>
                        <div class="card-action right-align">
                            <button type="submit" v-if="submit == 0" class="btn waves-effect waves-light">Submit</button>
                            <button type="type" v-if="submit == 1" class="btn waves-effect">Processing ...</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script>
    new Vue({
        el: '#app-vue',
        data: {
            api_url: '{{env("Client_API")}}',
            formLog: {
                name : '',
                email : '',
                phone: ''
            },
            submit : 0
        },
        methods: {
            login: function () {
                const _this = this;
                const URL = this.api_url+'/general/applicant/entry';
                _this.submit = 1;
                $.ajax({
                    url: URL,
                    type: "post",
                    data: _this.formLog,
                    success: function (res) {
                        _this.submit = 0;
                        console.log(res);
                        if(res.status === 2000){
                            M.toast({html: 'Successfully added to applicant list in'});
                            window.location.href = '{{env('APP_URL')}}/applicant/'+res.data.short;
                        } else if(res.status === 7000){
                            M.toast({html: 'Email is already been used. Please try new one.'});
                        } else {
                            M.toast({html: 'Please fill up the form correctly'});
                        }
                    }
                });
            }
        }
    });
</script>