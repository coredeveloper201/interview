<div id="app-question">
    <div class="container">
        <div class="col m8 offset-m2">
            <div class="card">
                <div class="card-content" v-if="applicant != null">
                    <div class="card-title center-align">
                        <span v-text="applicant.name"></span>
                        <a href="javascript:void(0)" @click="infoEdit = 1"><i class="material-icons" style="font-size: 13px">edit</i></a>
                    </div>
                    <div class="row">
                        <div v-if="infoEdit == 0">
                            <div class="col m6 offset-m3" v-if="applicant.email != null">
                                <p class="center-align">Email Address: <a :href="'mailto:'+applicant.email" target="_blank" v-text="applicant.email"></a></p>
                            </div>
                            <div class="col m6 offset-m3" v-if="applicant.phone != null">
                                <p class="center-align">Contact Number: <a :href="'tel:'+applicant.phone" target="_blank" v-text="applicant.phone"></a></p>
                            </div>
                            <div class="col m6 offset-m3" v-if="applicant.details_doc != null">
                                <p class="center-align">Personal Skills: <a :href="applicant.details_doc" target="_blank">Click Here</a></p>
                            </div>
                        </div>

                        <div class="col m6 offset-m3">
                            <form @submit.prevent="updatePersonalInfo">
                                <div class="input-field col s12" v-if="applicant.email == null || infoEdit == 1">
                                    <input id="email" type="email" v-model="infoEditData.email" class="validate">
                                    <label for="email" :class="{active: infoEditData.email != null ? true : false}">Email Address</label>
                                </div>
                                <div class="input-field col s12" v-if="applicant.phone == null || infoEdit == 1">
                                    <input id="phone" type="tel" class="validate" v-model="infoEditData.phone">
                                    <label for="phone" :class="{active: infoEditData.phone != null ? true : false}">Contact Number</label>
                                    <span class="helper-text center-align" data-error="wrong" data-success="right">
                                        Please provide your available contact number so that we can contact you anytime and you won't miss the call
                                    </span>
                                </div>
                                <div class="input-field col s12" v-if="applicant.details_doc == null || infoEdit == 1">
                                    <input id="doc" type="tel" class="validate" v-model="infoEditData.details_doc">
                                    <label for="doc" :class="{active: infoEditData.details_doc != null ? true : false}">Personal Skills Google Doc</label>
                                    <span class="helper-text center-align" data-error="wrong" data-success="right">
                                    Please attach a google document(doc/pdf/excel) url including all your skills.
                                    Here goes a sample/example format - <a
                                                href="https://drive.google.com/file/d/1uvQcU5AbE4gonfITRt2tQ14gThrTlTBq/view"
                                                target="_blank">Click here to see</a>
                                    </span>
                                </div>
                                <div class="input-field col s12 center-align">
                                    <button class="btn-flat waves-effect waves-light" type="button" v-if="infoEdit == 1" @click="infoEdit = 0">Cancel</button>
                                    <button class="btn waves-effect waves-light" type="submit" v-if="(applicant.email == null || applicant.phone == null || applicant.details_doc == null) || infoEdit == 1">Update Personal Info</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-content" v-if="questions.length > 0">
                    <div class="card-title center-align">
                        <span>Questions</span>
                    </div>
                    <form @submit.prevent="questionAnswer">
                        <div class="card-content">
                            <div class="row">
                                <div class="input-field col s12" v-for="(question, index) in questions">
                                    {{--<span v-text="question.title"></span>--}}
                                    <textarea :id="'q'+question.id" v-model="question.answer" class="materialize-textarea"></textarea>
                                    <label :for="'q'+question.id"  :class="{active: question.answer == null || question.answer == '' ? false : true}" v-text="question.title"></label>
                                    <span class="helper-text left-align pink-text text-darken-2" v-if="question.description != null" v-text="question.description"></span>
                                </div>
                            </div>
                        </div>
                        <div class="card-action right-align" v-if="questions.length > 0">
                            <button type="submit" class="btn waves-effect waves-light">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    new Vue({
        el: '#app-question',
        data: {
            api_url: '{{env("Client_API")}}',
            short_code : '{{$short_code}}',
            infoEdit: 0,
            infoEditData: {
                id: 0,
                email: '',
                phone: '',
                details_doc: '',
                short: '{{$short_code}}'
            },
            applicant : null,
            questions : []
        },
        methods: {
            getInfo: function () {
                const _this = this;
                const URL = this.api_url+'/general/applicant/'+_this.short_code;
                $.ajax({
                    url: URL,
                    type: "get",
                    success: function (res) {
                        if(parseInt(res.status) === 2000){
                            _this.applicant = res.data.applicant;
                            _this.questions = res.data.questions;
                            _this.infoEditData.id = _this.applicant.id;
                            _this.infoEditData.email = _this.applicant.email;
                            _this.infoEditData.phone = _this.applicant.phone;
                            _this.infoEditData.details_doc = _this.applicant.details_doc;
                        } else {
                            M.toast({html: 'Something Wrong. Please try again!'});
                        }
                    }
                });
            },
            updatePersonalInfo: function () {
                const _this = this;
                const URL = this.api_url+'/general/applicant/personal/update';
                $.ajax({
                    url: URL,
                    type: "post",
                    data: _this.infoEditData,
                    success: function (res) {
                        if(parseInt(res.status) === 2000){
                            _this.applicant = res.data;
                            _this.infoEdit = 0;
                            _this.infoEditData.id = _this.applicant.id;
                            _this.infoEditData.email = _this.applicant.email;
                            _this.infoEditData.phone = _this.applicant.phone;
                            _this.infoEditData.details_doc = _this.applicant.details_doc;
                        }
                        else if(parseInt(res.status) === 3000){
                            window.location.href = '{{env('APP_URL')}}/applicant/entry';
                        }
                        else if(parseInt(res.status) === 7000){
                            M.toast({html: 'Email is already been used. Please try new one.'});
                        } else {
                            M.toast({html: 'Something Wrong. Please try again!'});
                        }
                    }
                });
            },
            questionAnswer: function () {
                const _this = this;
                const URL = this.api_url+'/general/applicant/answers/'+_this.short_code;
                $.ajax({
                    url: URL,
                    type: "post",
                    data: {questions : _this.questions},
                    success: function (res) {
                        if(parseInt(res.status) === 2000){
                            _this.getInfo();
                        }else {
                            M.toast({html: 'Something Wrong. Please try again!'});
                        }
                    }
                });
            }
        },
        created(){
            this.getInfo();
        },
        mounted(){
            swal('Please keep this url saved to you for future uses. This url is only known to you.')
        }
    });
</script>